<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/pertanyaan');
});

Route::get('/pertanyaan', 'PertanyaanController@index')->name('home');

Route::get('/pertanyaan/create', 'PertanyaanController@create')->name('create');

Route::post('/pertanyaan', 'PertanyaanController@store')->name('store');

Route::delete('/pertanyaan/{pertanyaan_id}','PertanyaanController@destroy')->name('delete');

Route::get('/pertanyaan/{pertanyaan_id}/edit}', 'PertanyaanController@edit')->name('edit');
Route::put('/pertanyaan/{pertanyaan_id}','PertanyaanController@update')->name('update');
