<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PertanyaanController extends Controller
{
    public function index(){
        $pertanyaans = DB::table('pertanyaan')->get();
        return view('home',['title' => 'List Data', 'pertanyaans' => $pertanyaans]);

    }

    public function create(){
        return view('create',['title'=>'Tambah Data']);
    }

    public function store(Request $request){
        $rules = [
            'judul' => 'required|max:255',
            'isi' => 'required|max:255',
            'jawaban_tepat_id' => 'nullable',
            'profil_id' => 'nullable',
        ];

        $pesan = [
            'judul.required' => 'Masukkan Judul Dengan Benar!',
            'isi.required' => 'Masukkan Isi Dengan Benar!',
        ];

        $data = Validator::make($request->all(),$rules,$pesan);

        if($data->fails()){
            return redirect()->route('create')->withErrors($data)->withInput();
        } else {
            $db = DB::table('pertanyaan')->insert([
                'judul' => $request->judul,
                'isi' => $request->isi,
                'tanggal_dibuat'=>date('Y-m-d'),
                'tanggal_diperbarui'=>date('Y-m-d'),
                'jawaban_tepat_id' =>NULL,
                'profil_id' => NULL,
            ]);

            return redirect()->route('home')->with('pesan','Data Berhasil Ditambahkan');
        }
    }

    public function show(){

    }

    public function edit($id){
        $pertanyaans = DB::table('pertanyaan')->where('id',$id)->first();
        return view('edit',['data' => $pertanyaans,'title' => 'Edit Data']);
    }

    public function update(Request $request,$id){
        $rules = [
            'judul' => 'required|max:255',
            'isi' => 'required|max:255',
            'jawaban_tepat_id' => 'nullable',
            'profil_id' => 'nullable',
        ];

        $pesan = [
            'judul.required' => 'Masukkan Judul Dengan Benar!',
            'isi.required' => 'Masukkan Isi Dengan Benar!',
        ];

        $data = Validator::make($request->all(),$rules,$pesan);

        if($data->fails()){
            return redirect()->route('edit',['pertanyaan_id'=>$id])->withErrors($data)->withInput();
        } else {
            $db = DB::table('pertanyaan')->where('id',$id)->update(
                [
                    'judul' => $request->judul,
                    'isi' => $request->isi,
                    'tanggal_dibuat'=>date('Y-m-d'),
                    'tanggal_diperbarui'=>date('Y-m-d'),
                    'jawaban_tepat_id' =>NULL,
                    'profil_id' => NULL,
                ]
            );

            return redirect()->route('home')->with('pesan','update');
        }
    }

    public function destroy($id){
        $delete = DB::table('pertanyaan')->where('id',$id)->delete();
        return redirect()->route('home')->with('pesan','dihapus');
    }
}
