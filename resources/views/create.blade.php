@extends('layout.master')
@section('content')
<section class="mt-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="{{ route('store') }}" method="post">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Tambah Data</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="" class="font-weight-bold">Judul</label>
                                <input type="text" name="judul" id="" class="form-control @error('judul') is-invalid @enderror" placeholder="Masukkan Judul" value="{{old('judul')}}">
                                @error('judul')
                                    <div class="text-danger">
                                        <span>{{$message}}</span>
                                    </div>
                                @enderror
                              </div>
                              <div class="form-group">
                                <label for="" class="font-weight-bold">Isi</label>
                                <input type="text" name="isi" id="" class="form-control @error('isi') is-invalid @enderror" placeholder="Masukkan Isi" value="{{old('judul')}}">
                                @error('isi')
                                <div class="text-danger">
                                    <span>{{$message}}</span>
                                </div>
                                @enderror
                              </div>
                              <div class="form-group">
                                  <button type="submit" class="btn btn-primary">Tambah Data</button>
                              </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
