@extends('layout.master')
@section('content')
<section class="bg-white">
    <div class="container mt-4">
        @if (session('pesan') == 'Data Berhasil Ditambahkan')
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Data Berhasil Ditambahkan!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @elseif(session('pesan') == 'dihapus')
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Data Dihapus!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @elseif(session('pesan') == 'update')
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Data Berhasil Diperbarui</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @endif
        <div class="row">
            <div class="col-12 d-flex justify-content-between align-items-center">
                <h1>{{$title}}</h1>
                <a href="{{ route('create')}}" class="btn btn-primary">Tambah Data</a>
            </div>
            <div class="col-12">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>No</th>
                        <th>Judul</th>
                        <th>Isi</th>
                        <th>Tanggal Dibuat</th>
                        <th>Tanggal Diperbarui</th>
                        <th>Aksi</th>
                    </tr>
                    @forelse ($pertanyaans as $pertanyaan)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$pertanyaan->judul}}</td>
                            <td>{{$pertanyaan->isi}}</td>
                            <td>{{$pertanyaan->tanggal_diperbarui}}</td>
                            <td>{{$pertanyaan->tanggal_dibuat}}</td>

                            <td class="d-flex justify-content-around"><a href="{{ route('edit', ['pertanyaan_id'=>$pertanyaan->id]) }}" class="btn btn-success">Edit</a>
                                <form action="{{ route('delete', ['pertanyaan_id'=>$pertanyaan->id]) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('Apakah anda yakin?')">Delete</button>
                                </form>
                            </td>

                        </tr>
                    @empty
                    <h1 class="text-center">Tidak Ada Data</h1>
                    @endforelse
                </table>
            </div>
        </div>
    </div>
  </section>
@endsection
